package jtm.activity09;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

/*- TODO #2
 * Implement Iterator interface with Orders class
 * Hint! Use generic type argument of iterateable items in form: Iterator<Order>
 * 
 * TODO #3 Override/implement public methods for Orders class:
 * - Orders()                — create new empty Orders
 * - add(Order item)            — add passed order to the Orders
 * - List<Order> getItemsList() — List of all customer orders
 * - Set<Order> getItemsSet()   — calculated Set of Orders from list (look at description below)
 * - sort()                     — sort list of orders according to the sorting rules
 * - boolean hasNext()          — check is there next Order in Orders
 * - Order next()               — get next Order from Orders, throw NoSuchElementException if can't
 * - remove()                   — remove current Order (order got by previous next()) from list, throw IllegalStateException if can't
 * - String toString()          — show list of Orders as a String
 * 
 * Hints:
 * 1. To convert Orders to String, reuse .toString() method of List.toString()
 * 2. Use built in Collections.sort(...) method to sort list of orders
 * 
 * TODO #4
 * When implementing getItemsSet() method, join all requests for the same item from different customers
 * in following way: if there are two requests:
 *  - ItemN: Customer1: 3
 *  - ItemN: Customer2: 1
 *  Set of orders should be:
 *  - ItemN: Customer1,Customer2: 4
 */

public class Orders implements Iterator {
	List<Order> itemsList;
	Iterator<Order> iter;
	int numberOfCurrentOrder;

	public Orders() {
		itemsList = new ArrayList<Order>();
		numberOfCurrentOrder = -1;
		iter = itemsList.iterator();
	}

	public Iterator<Order> getIter() {
		return itemsList.iterator();
		// return iter;
	}

	public List<Order> getItemsList() {
		return itemsList;
	}

	public boolean alreadyExists(String name, Set<Order> orderSet) {
		for (Order order : orderSet) {
			if (order.getName().equals(name))
				return true;
		}
		return false;
	}

	public Set<Order> getItemsSet() {
		Set<Order> orderSet = new TreeSet<Order>();
		List<Order> items = itemsList;

		for (int i = 0; i < items.size(); i++) {
			Order temp = items.get(i);
			Order temp2 = temp;
			String customer = temp.getCustomer();
			int numCount = temp.getCount();
			if (!alreadyExists(temp.getName(), orderSet)) {

				for (int j = i + 1; j < items.size(); j++) {
					Order order = items.get(j);

					if (temp.getName().equals(order.getName())) {
						// temp2 = new Order(temp.getCustomer() + "," + order.getCustomer(),
						// temp.name,temp.getCount() + order.getCount() );
						customer = order.getCustomer() + "," + customer;
						numCount += order.getCount();

					}

				}
				temp2 = new Order(customer, temp.getName(), numCount);
				orderSet.add(temp2);

			}
		}
		return orderSet;
	}

	public void add(Order item) {
		itemsList.add(item);
		numberOfCurrentOrder++;
	}

	@Override
	public void remove() {
		if (!itemsList.isEmpty()) {
			itemsList.remove(itemsList.size() - 1);
			// iter.remove();
			numberOfCurrentOrder--;
		} else
			throw new IllegalStateException("List is empty");

	}

	public void sort() {
		Collections.sort(itemsList);
	}

	@Override
	public String toString() {
		return itemsList.toString();
	}

	@Override
	public boolean hasNext() {
		return itemsList.iterator().hasNext();
		// return iter.hasNext();
	}

	@Override
	public Order next() {
		return itemsList.iterator().next();
		// return iter.next();
	}

	/*-
	 * TODO #1
	 * Create data structure to hold:
	 *   1. some kind of collection of Orders (e.g. some List)
	 *   2. index to the current order for iterations through the Orders in Orders
	 *   Hints:
	 *   1. you can use your own implementation or rely on .iterator() of the List
	 *   2. when constructing list of orders, set number of current order to -1
	 *      (which is usual approach when working with iterateable collections).
	 */

}
