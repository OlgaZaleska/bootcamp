package jtm.activity09;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/*- TODO #1
 * Implement Comparable interface with Order class
 * Hint! Use generic type of comparable items in form: Comparable<Order>
 * 
 * TODO #2 Override/implement necessary methods for Order class:
 * - public Order(String orderer, String itemName, Integer count) — constructor of the Order
 * - public int compareTo(Order order) — comparison implementation according to logic described below
 * - public boolean equals(Object object) — check equality of orders
 * - public int hashCode() — to be able to handle it in some hash... collection 
 * - public String toString() — string in following form: "ItemName: OrdererName: Count"
 * 
 * Hints:
 * 1. When comparing orders, compare their values in following order:
 *    - Item name
 *    - Customer name
 *    - Count of items
 * If item or customer is closer to start of alphabet, it is considered "smaller"
 * 
 * 2. When implementing .equals() method, rely on compareTo() method, as for sane design
 * .equals() == true, if compareTo() == 0 (and vice versa).
 * 
 * 3. Also Ensure that .hashCode() is the same, if .equals() == true for two orders.
 * 
 */

public class Order implements Comparable<Order> {
	String customer; // Name of the customer
	String name; // Name of the requested item
	int count; // Count of the requested items

	// constructor of the Order
	public Order(String customer, String name, Integer count) {
		this.customer = customer;
		this.name = name;
		this.count = count;
	}

	public String getCustomer() {
		return customer;
	}

	public void setCustomer(String customer) {
		this.customer = customer;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	@Override
	public int compareTo(Order order) {

		int comparator = 0;
		int custLength = (customer.length() < order.customer.length()) ? customer.length() : order.customer.length();

		for (int i = 0; i < custLength; i++) {

			if (customer.charAt(i) > order.customer.charAt(i)) {
				comparator += customer.charAt(i) - order.customer.charAt(i);
			} else if (customer.charAt(i) < order.customer.charAt(i))
				comparator += customer.charAt(i) - order.customer.charAt(i);
			else
				comparator += 0;
		}
		if (customer.length() < order.customer.length())
			comparator += order.customer.charAt(custLength);
		else if (customer.length() < order.customer.length())
			comparator += customer.charAt(custLength);

		int nameLength = 0;
		if (name.length() < order.name.length()) {
			nameLength = name.length();
			comparator += order.name.charAt(nameLength);

		} else if (name.length() > order.name.length()) {
			nameLength = order.name.length();
			comparator += name.charAt(nameLength);
		}

		for (int i = 0; i < nameLength; i++) {
			if (name.charAt(i) > order.name.charAt(i)) {
				comparator += name.charAt(i) - order.name.charAt(i);
			} else if (name.charAt(i) < order.name.charAt(i))
				comparator += name.charAt(i) - order.name.charAt(i);
			else
				comparator += 0;
		}
		if (count < order.count) {
			comparator += count - order.count;
		} else if (count > order.count)
			comparator += count - order.count;

		else
			comparator += 0;

		return comparator;

	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Order) {
			int result = compareTo((Order) obj);
			if (result == 0)
				return true;
			else
				return false;
		} else
			return false;
	}

	@Override
	public int hashCode() {

		return name.hashCode() + customer.hashCode() + count;
	}


	@Override
	public String toString() {

		return name + ": " + customer + ": " + count;
	}

	public static void main(String[] args) {

		Order order = new Order("Olga", "Pizza", 20);
		Order order2 = new Order("Alisa", "Egg", 10);
		Order order6 = new Order("Alisa", "Egg2", 10);
		Order order3 = new Order("Olga2", "Pizza", 20);
		Order order4 = new Order("Nata", "Pizza", 5);

		Orders ordList = new Orders();
		ordList.add(order);
		ordList.add(order2);
		ordList.add(order3);
		ordList.add(order6);
		ordList.add(order4);
		Set<Order> hs = new HashSet<Order>();
		hs = ordList.getItemsSet();

		for (Order order5 : hs) {
			System.out.println(order5.toString());
		}
		Iterator<Order> iter = ordList.getIter();
		while (iter.hasNext())
			System.out.println(iter.next());

		System.out.println(order2.compareTo(order6));
		System.out.println(order2.equals(order6));
	}
}
