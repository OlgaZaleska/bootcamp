package jtm.activity04;

import java.util.Locale;

public class Transport {
	// Do not change access modifiers to encapsulate internal properties!
	private String id; // Transport registration number
	private float consumption; // fuel consumption in litres per 100km
	private int tankSize; // tank size in litres
	private float fuelInTank; // fuel in tank

	/*- TODO #1
	 * Select menu Source — Generate Constructor using Fields...
	 * and create constructor which sets id, consumption, tankSize
	 * values of the newly created object
	 * And make fuel tank full.
	 */
	public Transport(String id, float consumption, int tankSize/* , float fuelInTank */) {
		this.id = id;
		this.consumption = consumption;
		this.tankSize = tankSize;
		this.fuelInTank = tankSize;
	}

	/*- TODO #2
	 * Select menu: Source — Generate getters and Setters...
	 * and generate public getters for consumption, tankSize, id, and
	 * fuelInTank fields
	 */
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public float getConsumption() {
		return consumption;
	}

	public void setConsumption(float consumption) {
		this.consumption = consumption;
	}

	public int getTankSize() {
		return tankSize;
	}

	public void setTankSize(int tankSize) {
		this.tankSize = tankSize;
	}

	public float getFuelInTank() {
		return fuelInTank;
	}

	/*- TODO #3
	 * Select menu: Source — Generate toString()...
	 * and implement this method, that returns String in form:
	 * "Id:ID cons:0.0l/100km, tank:00l, fuel:00.00l"
	 * where ID and numbers are actual properties of the transport
	 * HINT: use String.format(Locale.US, "%.2f", float) to limit shown digits
	 * to 2 decimal for fractions, and dot as a decimal delimiter.
	 */
	@Override
	public String toString() {
		return "Id:" + id + " cons:" + String.format(Locale.US, "%.1f", consumption) + "l/100km" + ", tank:" + tankSize
				+ "l, fuel:" + String.format(Locale.US, "%.2f", fuelInTank) + "l";
	}

	// Return transport id and type as string e.g. "AAA Transport"
	// HINT: use this.getClass().getSimpleName(); to get type of transport
	protected final String getType() {
		// TODO return required value
		return this.getClass().getSimpleName();
	}

	public void setFuelInTank(float fuelInTank) {
		if (fuelInTank <= tankSize)
			this.fuelInTank = fuelInTank;
		else {
			fuelInTank = tankSize;
			System.out.println(" Wrong fuel amount, set tank size");
		}
	}

	// HINT: use getType() to describe transport and road.toString() to describe
	// road
	// HINT: String.format(Locale.US, "%.2f", float) to format float number with
	// fixed mask
	public String move(Road road) {
		// TODO If transport has enough fuel, decrease actual amount of fuel by
		// necessary amount and return String in form:
		// "AAA Type is moving on From–To, 180km"
		// TODO If there is no enough fuel in tank, return string in form:
		// "Cannot move on From–To, 180km. Necessary
		// fuel:0.00l, fuel in tank:0.00l"

		float requiredFuel;
		requiredFuel = (road.getDistance() * consumption / 100);

		if (requiredFuel <= fuelInTank) {
			fuelInTank = fuelInTank - requiredFuel;

			return id + " " + getType() + " is moving on " + road.getFrom() + " — " + road.getTo() + ", "
					+ road.getDistance() + "km";

		}

		// else if (requiredFuel > fuelInTank)
		else
			return "Cannot move on " + road.getFrom() + " — " + road.getTo() + ", " + road.getDistance()
					+ "km. Necessary fuel:" + String.format(Locale.US, "%.2f", requiredFuel) + "l, fuel in tank:"
					+ String.format(Locale.US, "%.2f", fuelInTank) + "l";

	}

	public static void main(String[] args) {
		Transport taxi = new Transport("1111", 8f, 12); // ID, consuption, fuelIntheTank
		Transport bus = new Transport("2222", 12f, 60);
		Transport truck = new Transport("3333", 25.5f, 80);
		System.out.println(bus);
		System.out.println(bus.getType());

		Road route1 = new Road("Riga", "Ventspils", 180);// from, to distance
		Road route2 = new Road("Riga", "Minsk", 600);
		Road route3 = new Road("Riga", "Jurmala", 25);

		System.out.println(taxi.move(route1));
		System.out.println(bus.move(route2));
		System.out.println(truck.move(route3));

	}
}
