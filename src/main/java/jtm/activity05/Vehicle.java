package jtm.activity05;

import jtm.activity04.Road;
import jtm.activity04.Transport;

public class Vehicle extends Transport {

	protected int wheels; // 1. Allow to store protected int number of wheels for vehicle.

	// 2. Implement Vehicle(String id, float consumption, int tankSize, int wheels)
	// constructor,
	public Vehicle(String id, float consumption, int tankSize, int wheels) {
		super(id, consumption, tankSize);
		this.wheels = wheels;
	}

	@Override
	public String move(Road road) {

		String moveStr = super.move(road);

		if (road.getClass() == Road.class)
			return moveStr.replace("moving", "driving") + " with " + wheels + " wheels";

		else
			return ("Cannot drive on " + road);
	}

	public static void main(String[] args) {
		Vehicle vehicle = new Vehicle("777", 20f, 100, 4);

		Road route2 = new WaterRoad("Riga", "Minsk", 600);
		Road route3 = new Road("Riga", "Jurmala", 25);

		System.out.println(vehicle.move(route2));
		System.out.println(vehicle.move(route3));
	}
}