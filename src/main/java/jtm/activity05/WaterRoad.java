package jtm.activity05;

import jtm.activity04.Road;

public class WaterRoad extends Road {

	public WaterRoad() {
		super();

	}

	public WaterRoad(String from, String to, int distance) {
		super(from, to, distance);

	}
	// 5. Select Source — Override/Implement methods... and select method
	// toString().
	// 6. Override .toString() method which returns string in form: WaterRoad From —
	// To, 00km

	@Override
	public String toString() {
		
		return getClass().getSimpleName() + " " + super.toString();
	}

}
