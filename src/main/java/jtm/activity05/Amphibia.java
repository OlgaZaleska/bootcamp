package jtm.activity05;

import jtm.activity04.Road;
import jtm.activity04.Transport;


public class Amphibia extends Transport {
	//1.Make all internal properties of Amphibia private.
	private Ship ship;
	private Vehicle vehicle;
	//2.Implement constructor Amphibia(String id, float consumption, int tankSize, byte sails, int wheels)
	
	public Amphibia(String id, float consumption, int tankSize, byte sails, int wheels) {
		super(id, consumption, tankSize);
		ship = new Ship(id, sails);
		vehicle = new Vehicle (id, consumption, tankSize, wheels);
	}
	
	//3.Override move(Road road) method, that Amhibia behaves like a Vehicle on ground 
		//road and like a Ship on water road.
	@Override
	public String move(Road road) {
		String moveStr = super.move(road);
		
		if (road instanceof WaterRoad)
			return moveStr.replace("moving", "sailing") + " with " + ship.sails + " sails";
					
		else
						return moveStr.replace("moving", "driving") + " with " + vehicle.wheels + " wheels";
			
	}
	
	public static void main(String[] args) {
		
		Amphibia ship = new Amphibia("888", 20f, 100, (byte)2, 4);
		System.out.println(ship);
		Amphibia vehicle = new Amphibia("888", 20f, 100, (byte)2, 4);
		System.out.println(vehicle.getType());
		
		WaterRoad route1 = new WaterRoad("Riga", "Ventspils", 180);// from, to distance
		Road route2 = new Road("Riga", "Minsk", 600);
		Road route3 = new Road("Riga", "Jurmala", 25);
		
		System.out.println(ship.move(route1));
		System.out.println(vehicle.move(route2));
		System.out.println(vehicle.move(route3));
	}
	
	
	
	}


