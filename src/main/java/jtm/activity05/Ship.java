package jtm.activity05;

import jtm.activity04.Road;
import jtm.activity04.Transport;

public class Ship extends Transport { //	Create class Ship as a subclass of Transport
	
	protected byte sails;

	public Ship(String id, byte sails) { //	Create Ship(String id, byte sails) constructor
		super(id,0,0);
		this.sails = sails;
	}

	
	@Override
	public String move(Road road) { //3.	Override move(Road) to return String in form
				//return super.move(road);
		if(road instanceof WaterRoad)
			return super.getType() + " is sailing on "  + road +  " with " + this.sails + " sails";
		else
			return ("Cannot sail on " + road);
	}
		
		
	//3.	Override move(Road) to return String in form:
		//ID Ship is sailing on (Road as String) with x sails
	//	where:
	//		(Road as String) is string representation of the road (without brackets),
	//		x is actual number of sails.

	public static void main(String[] args) {
		Ship ship = new Ship("888", (byte)5);
		System.out.println(ship.move(new WaterRoad("Riga", "Liepaja", 150 )));
		
	}
}
