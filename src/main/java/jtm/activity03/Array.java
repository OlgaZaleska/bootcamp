package jtm.activity03;

import java.util.Arrays;

public class Array {
	static int[] array;

	public static void main(String[] args) {
		// TODO Use passed parameters for main method to initialize array
		// Hint: use Run— Run configurations... Arguments to pass parameters to
		// main method when calling from Eclipse
		// Sort elements in this array in ascending order
		// Hint: use Integer.parseInt(args[n]) to convert passed
		// parameters from String to int
		// Hint: use Arrays.sort(...) from
		// https://docs.oracle.com/javase/7/docs/api/java/util/Arrays.html

		System.out.print("String array: ");
		String result = "";
		for (String string : args) {

			System.out.print(string + " ");

			result += string;
		}
		System.out.println();
		String[] s = result.split(""); // this is an array of strings
		array = new int[s.length];// create a new array of the type int
		System.out.print("Integer array: ");
		for (int i = 0; i < s.length; i++) {
			array[i] = Integer.parseInt(s[i]); // parse each string integer
			System.out.print(s[i] + " ");
		}
		int[] array2 = returnSortedArray();
		for (int i = 0; i < array.length; i++)

			System.out.println();
		printSortedArray();
	}

	public static void printSortedArray() {
		// TODO print content of array on standard output
		// Hint: use Arrays.toString(array) method for this
		Arrays.sort(array);
		System.out.print("Sorted array " + Arrays.toString(array));
	}

	public static int[] returnSortedArray() {
		// TODO return reference to this array
		Arrays.sort(array);
		return array;
	}
}
