package jtm.extra01;

public class Zodiac {

	/**
	 * Determine the sign of the zodiac, use day and month.
	 * 
	 * @param day
	 * @param month
	 * @return zodiac
	 */
	public static String getZodiac(int day, int month) {
		String zodiac = null;

		if ((day >= 21 && month == 3) || (day < 20 && month == 4))
			zodiac = "ARIES";
		else if ((day >= 20 && month == 4) || (day < 21 && month == 5))
			zodiac = "TAURUS";
		else if ((day >= 21 && month == 5) || (day < 21 && month == 6))
			zodiac = "GEMINI";
		else if ((day >= 21 && month == 6) || (day < 23 && month == 7))
			zodiac = "CANCER";
		else if ((day >= 23 && month == 7) || (day < 23 && month == 8))
			zodiac = "LEO";
		else if ((day >= 23 && month == 8) || (day < 23 && month == 9))
			zodiac = "VIRGO";
		else if ((day >= 23 && month == 9) || (day < 23 && month == 10))
			zodiac = "LIBRA";
		else if ((day >= 23 && month == 10) || (day < 22 && month == 11))
			zodiac = "SCORPIO";
		else if ((day >= 22 && month == 11) || (day < 22 && month == 12))
			zodiac = "SAGITTARIUS";
		else if ((day >= 22 && month == 12) || (day < 20 && month == 1))
			zodiac = "CAPRICORN";
		else if ((day >= 20 && month == 1) || (day < 19 && month == 2))
			zodiac = "AQUARIUS";
		else
			zodiac = "PISCES";
		// TODO #1: Implement method which return zodiac sign names
		// As method parameter - day and month;
		// Look at wikipedia:
		// https://en.wikipedia.org/wiki/Zodiac#Table_of_dates
		// Tropical zodiac, to get appropriate date ranges for signs
		return zodiac;
	}

	public static void main(String[] args) {
		// HINT: you can use main method to test your getZodiac method with
		// different parameters
		System.out.println(getZodiac(40, 4));

	}

}
