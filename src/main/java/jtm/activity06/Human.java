package jtm.activity06;

public class Human implements Humanoid {

	int stomach;
	boolean isAlive;

	public Human() {
		stomach = 0;
		isAlive = true;

	}

	public Human(int stomach, boolean isAlive) {

		this.stomach = stomach;
		this.isAlive = isAlive;
	}

	@Override
	public void eat(Integer food) {

		if (stomach == 0)
			stomach += food;

	}

	@Override
	public Integer vomit() {
		int temp = stomach; // fixing eaten food
		stomach = 0; // make stomach empty
		return temp; // return eaten food from the stomach*/

	}

	@Override
	public String isAlive() {
		if (isAlive)
			return "Alive";
		return "Dead";
	}

	@Override
	public String killHimself() {
		isAlive = false;
		return "Dead";
	}

	@Override
	public int getWeight() {
		// TODO Auto-generated method stub
		return BirthWeight + stomach;
	}

	@Override
	public String toString() {
		return getClass().getSimpleName() + ": " + getWeight() + " [" + stomach + "]";

	}

}
