package jtm.activity06;

public class Martian implements Alien, Humanoid, Cloneable {

	Object stomach;
	int weight;

	public Martian(Object stomach, int weight) {

		this.stomach = stomach;
		this.weight = weight;

	}

	public Martian() {
		stomach = null;
		weight = Alien.BirthWeight;
	}
	/*
	 * If Alien is hungry (stomach is empty), eat Object. Possibly eaten items are:
	 * 
	 * 1. Integer, 2. Humanoid, 3. Alien.
	 * 
	 * Gain weight of eaten item, and kill it, if possible
	 */

	@Override
	public void eat(Object item) {

		if (stomach == null) {
			stomach = item;
			if (item instanceof Alien)
				weight += ((Alien) item).getWeight();
			else if (item instanceof Humanoid) {
				((Humanoid) stomach).killHimself();

				weight += ((Humanoid) item).getWeight();
			} else if (item instanceof Integer) {
				Integer tmp = (Integer) item;
				weight += tmp;
			}

		}
	}

	@Override
	public void eat(Integer food) {
		eat((Object) food);
	}

	@Override
	public Object vomit() {

		Object temp = null;

		if (stomach instanceof Martian) {

			weight -= ((Martian) stomach).getWeight();
			try {
				temp = ((Martian) stomach).clone();
			} catch (CloneNotSupportedException e) {

				e.printStackTrace();
			}

		} else if (stomach instanceof Human) {
			weight -= ((Human) stomach).getWeight();
			temp = new Human(((Human) stomach).stomach, false);

		} else if (stomach instanceof Integer) {
			weight -= (int) stomach;
			temp = new Integer((int) stomach);

		}
		stomach = null;
		return temp;
	}

	@Override
	public int getWeight() {

		return weight;
	}

	@Override
	public String isAlive() {
		return "I AM IMMORTAL!";
	}

	@Override
	public String killHimself() {
		return "I AM IMMORTAL!";
	}

	@Override
	public Object clone() throws CloneNotSupportedException {
		return clone(this);
	}

	private Object clone(Object current) {
		if (current instanceof Integer)
			return new Integer((Integer) current);

		else if (current instanceof Human) {
			Human newHuman = new Human();
			newHuman.stomach = ((Human) current).stomach;
			return newHuman;

		} else if (current instanceof Martian) {
			Martian newMartian = new Martian();
			newMartian.stomach = ((Martian) current).stomach;
			newMartian.weight = ((Martian) current).getWeight();
			return newMartian;

		} else
			return null;
	}

	@Override
	public String toString() {

		return this.getClass().getSimpleName() + ": " + weight + " [" + stomach + "]";
	}

	public static void main(String[] args) throws CloneNotSupportedException {
		Martian obj = new Martian();
		Martian martian = new Martian();
		Human human = new Human();
		human.eat(10);
		// obj.eat(15);
		martian.eat(human);
		obj.eat(martian);
		System.out.println(obj);
		System.out.println(obj.vomit() + " vomit");
//System.out.println(obj);

		// Martian obj2 = obj2.clone(obj);
		Martian obj2 = new Martian();
		// obj2= null;
		obj2 = (Martian) obj.clone();

		System.out.println(obj2);
	}
}
