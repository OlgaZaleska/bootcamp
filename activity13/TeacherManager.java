package jtm.activity13;

import static jtm.testsuite.AllTests.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

public class TeacherManager {

	protected Connection conn;

	public TeacherManager() throws Exception {
		/*-
		 * TODO #1 When new TeacherManager is created, create connection to the database server:
		 * - url = "jdbc:mysql://localhost/?autoReconnect=true&serverTimezone=UTC&characterEncoding=utf8"
		 * - user = "student"
		 * - pass = "Student007"
		 * Notes:
		 * 1. Use database name imported from jtm.testsuite.AllTests.database
		 * 2. Do not pass database name into url, because some statements in tests need to be executed
		 * server-wise, not just database-wise.
		 * 3. Set AutoCommit to false and use conn.commit() where necessary in other methods
		 */

		conn = null;
		Class.forName("com.mysql.cj.jdbc.Driver");
		conn = DriverManager.getConnection("jdbc:mysql://127.0.0.1/teachermanager", "root", "");
		conn.setAutoCommit(false);

	}

	/**
	 * Returns a Teacher instance represented by the specified ID.
	 * 
	 * @param id the ID of teacher
	 * @return a Teacher object
	 * @throws Exception
	 * 
	 */

	public Teacher findTeacher(int id) {
		// TODO #2 Write an sql statement that searches teacher by ID.
		// If teacher is not found return Teacher object with zero or null in
		// its fields!
		// Hint: Because default database is not set in connection,
		// use full notation for table "TeacherManager.Teacher"

		try {
			Statement statement = conn.createStatement();
			ResultSet resultTeacher = statement.executeQuery("SELECT * FROM TeacherManager.teacher WHERE id = " + id);
			if (resultTeacher.next()) {
				if (resultTeacher.getInt("id") == id) {

					int idi = Integer.parseInt("" + resultTeacher.getInt("id"));
					return new Teacher(idi, resultTeacher.getString("firstname"), resultTeacher.getString("lastname"));
				}
			}
		} catch (SQLException e) {
			return new Teacher();

		}
		return new Teacher();
	}

	/**
	 * Returns a list of Teacher object that contain the specified first name and
	 * last name. This will return an empty List of no match is found.
	 * 
	 * @param firstName the first name of teacher.
	 * @param lastName  the last name of teacher.
	 * @return a list of Teacher object.
	 * @throws Exception
	 */
	public List<Teacher> findTeacher(String firstName, String lastName) throws Exception {
		// TODO #3 Write an sql statement that searches teacher by first and
		// last name and returns results as ArrayList<Teacher>.
		// Note that search results of partial match
		// in form ...like '%value%'... should be returned
		// Note, that if nothing is found return empty list!
		List<Teacher> teachers = new ArrayList<Teacher>();
		Statement statement = conn.createStatement();
		ResultSet resultTeachers = statement.executeQuery("SELECT * FROM TeacherManager.Teacher WHERE firstName LIKE '%"
				+ firstName + "%'" + " AND lastName LIKE '%" + lastName + "%'");
		while (resultTeachers.next()) {
			teachers.add(new Teacher(resultTeachers.getInt("id"), resultTeachers.getString("firstName"),
					resultTeachers.getString("lastName")));

		}
		return teachers;

	}

	/**
	 * Insert an new teacher (first name and last name) into the repository.
	 * 
	 * @param firstName the first name of teacher
	 * @param lastName  the last name of teacher
	 * @return true if success, else false.
	 * @throws Exception
	 */

	public boolean insertTeacher(String firstName, String lastName) {
		// TODO #4 Write an sql statement that inserts teacher in database.

		try {
			String sqlInsert = ("INSERT INTO TeacherManager.Teacher (firstname, lastname) VALUES (?,?)");
			PreparedStatement statement = conn.prepareStatement(sqlInsert);
			statement.setString(1, firstName);
			statement.setString(2, lastName);
			statement.executeUpdate();
			conn.commit();
			return true;
		} catch (SQLException e) {
			return false;
		}
	}

	/**
	 * Insert teacher object into database
	 * 
	 * @param teacher
	 * @return true on success, false on error (e.g. non-unique id)
	 * @throws SQLException
	 */
	public boolean insertTeacher(Teacher teacher) throws SQLException {
		// TODO #5 Write an sql statement that inserts teacher in database.

		try {
			if (teacher != null && teacher.getFirstName() != null && teacher.getLastName() != null) {
				String sqlInsert = ("INSERT INTO TeacherManager.teacher (id, firstname, lastname) VALUES (?,?,?)");
				PreparedStatement statement = conn.prepareStatement(sqlInsert);
				ResultSet resultTeacher = statement
						.executeQuery("SELECT * FROM TeacherManager.teacher WHERE id = " + teacher.getId());
				if (resultTeacher.next())
					return false;
				statement.setInt(1, teacher.getId());
				statement.setString(2, teacher.getFirstName());
				statement.setString(3, teacher.getLastName());
				statement.executeUpdate();
				conn.commit();
				return true;
			} else
				return false;
		} catch (SQLException e) {
			return false;
		}
	}

	/**
	 * Updates an existing Teacher in the repository with the values represented by
	 * the Teacher object.
	 * 
	 * @param teacher a Teacher object, which contain information for updating.
	 * @return true if row was updated.
	 * @throws Exception
	 */
	public boolean updateTeacher(Teacher teacher) {

		boolean status = false;
		try {
			String sqlUpdate = "UPDATE TeacherManager.teacher set firstname = ?, lastname = ? where id = ?";
			PreparedStatement statement = conn.prepareStatement(sqlUpdate);
			statement.setInt(3, teacher.getId());
			statement.setString(1, teacher.getFirstName());
			statement.setString(2, teacher.getLastName());
			if (statement.executeUpdate() > 0) {
				conn.commit();
				return true;
			} else
				return false;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;

		}
	}

	/**
	 * Delete an existing Teacher in the repository with the values represented by
	 * the ID.
	 * 
	 * @param id the ID of teacher.
	 * 
	 * @return true if row was deleted.
	 * 
	 * @throws Exception
	 */
	public boolean deleteTeacher(int id) throws SQLException {
		// TODO #7 Write an sql statement that deletes teacher from database.

		try {
			String sqlDelete = "DELETE FROM TeacherManager.teacher WHERE id = ?";
			PreparedStatement statement = conn.prepareStatement(sqlDelete);
			statement.setInt(1, id);

			if (statement.executeUpdate() > 0) {
				conn.commit();
				return true;
			} else
				return false;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}

	}

	public void closeConnecion() throws Exception {
		// TODO Close connection to the database server

		if (conn != null) {
			try {
				conn.close();
				conn = null;
			} catch (Exception ignored) {
			}

		}
	}

}